const fs = require('fs');

function readLipsumFile(lipsum, callback) {
    //Subproblem - 1:Read the lipsum.txt file
    fs.readFile(lipsum, 'utf-8', (error, data) => {
        if (error) {
            callback(error);
            return;
        } else {
            console.log(data);

            //Subproblem - 2:Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt

            let dataInUpperCase = data.toUpperCase();
            let upperCasePath = './upperCaseContent.txt';
            fs.writeFile(upperCasePath, dataInUpperCase, (error) => {
                if (error) {
                    callback(error);
                    return;
                } else {
                    console.log("Data converted to UpperCase and write successfully to upperCaseContent.txt File");
                    fileNameCollector(upperCasePath);

                    //Subproblem - 3: Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt

                    fs.readFile(upperCasePath, 'utf-8', (error, data) => {
                        if (error) {
                            callback(error);
                            return;
                        } else {
                            const lowerCaseSplitPath = './lowerCaseContent.txt';
                            let lowerCaseData = data.toLowerCase();
                            let splitData = lowerCaseData.split('.');
                            fs.writeFile(lowerCaseSplitPath, splitData.toString(), (error) => {
                                if (error) {
                                    callback(error);
                                    return;
                                } else {
                                    console.log("Writing the lower-case splitted data into new file done successfully");
                                    fileNameCollector(lowerCaseSplitPath);

                                    //Subproblem -4:Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt

                                    fs.readFile(lowerCaseSplitPath, 'utf-8', (error, lowerCaseData) => {
                                        if (error) {
                                            callback(error);
                                            return;
                                        } else {
                                            let lines = lowerCaseData.split('\n');
                                            let sortedData = lines.sort();
                                            let finalData = sortedData.join('\n');
                                            let sortedDataPath = './sortedData.txt';
                                            fs.writeFile(sortedDataPath, finalData, (error) => {
                                                if (error) {
                                                    callback(error);
                                                    return;
                                                } else {
                                                    console.log("Writing the sorted data to new file done successfully");
                                                    fileNameCollector(sortedDataPath);

                                                    //Subproblem -5: Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.

                                                    fs.readFile('./filenames.txt', 'utf8', (error, fileNamesData) => {
                                                        if (error) {
                                                            callback(error);
                                                            return;
                                                        } else {
                                                            const fileNames = fileNamesData.split('\n');
                                                            fileNames.map((fileName) => {
                                                                fs.rm(fileName, (error) => {
                                                                    if (error) {
                                                                        callback(error);
                                                                        return;
                                                                    } else {
                                                                        console.log(`${fileName} is deleted successfully.`);
                                                                    }
                                                                });
                                                            })

                                                        }

                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });

                }
            });

        }
    });

}

function fileNameCollector(data) {
    fs.appendFile("./filenames.txt", data, (error) => {
        if (error) {
            console.error(error);
            return;
        }
        else {
            console.log(`${data} added to filenames.txt`);
        }
    });
}

module.exports = readLipsumFile;
