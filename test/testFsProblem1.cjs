const path = require("path");

const testFsProblem1 = require('../fs-problem1.cjs');

let randomNumberOfFiles = Math.ceil(Math.random() * 10);

testFsProblem1(path.join(__dirname, '/jsonFilesDirectory'), randomNumberOfFiles);