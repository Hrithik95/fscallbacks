const fs = require('fs');

//function to create random directory and then create_and_delete random number of json files in it
function createDirectoryAndFiles(directory, randomNumberOfFiles, callbackFunction) {
    let fileCount = 0;
    //creating random directory to store json files
    fs.mkdir(directory, (error) => {
        if (error) {
            console.error(error);
        } else {
            console.log("Directory created successfully!");

            //creating random number of json files inside random directory
            for (let index = 1; index <= randomNumberOfFiles; index++) {
                fs.writeFile(`${directory}/randomFile-${index}.json`, "", (error) => {
                    if (error) {
                        callbackFunction(error);
                    } else {
                        fileCount++;
                        console.log("file created successfully", fileCount);
                        if (fileCount >= randomNumberOfFiles) {
                            console.log("All files created successfully!");
                            callbackFunction(null);
                        }
                    }
                });
            }

        }
    });
}

// Implementing deleteFiles function to delete the json files present in random directory
function deleteFiles(directory, randomNumberOfFiles, callbackFunction) {
    let filesDeleted = 0;
    for (let index = 1; index <= randomNumberOfFiles; index++) {
        let filePath = `${directory}/randomFile-${index}.json`;
        fs.unlink(filePath, (error) => {
            if (error) {
                callbackFunction(error);
            } else {
                console.log(`File no. ${index} removed successfully`);
                filesDeleted++;
                if (filesDeleted === randomNumberOfFiles) {
                    console.log(filesDeleted);
                    fs.rmdir(directory, (error) => {
                        if (error) {
                            callbackFunction(error);;
                        } else {
                            console.log("Folder deleted successfully");
                            callbackFunction(null);
                        }
                    });
                }
            }
        });
    }
}


function fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles) {
    createDirectoryAndFiles(absolutePathOfRandomDirectory, randomNumberOfFiles, (error) => {
        if (error) {
            console.error(error);
        } else {
            deleteFiles(absolutePathOfRandomDirectory, randomNumberOfFiles, (error) => {
                if (error) {
                    console.error(error);
                } else {
                    console.log("All files and folder have been deleted successfully.");
                }
            });
        }
    });
}

module.exports = fsProblem1;